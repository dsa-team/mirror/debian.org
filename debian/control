Source: debian.org
Section: local/admin
Priority: required
Maintainer: Debian sysadmin Team <dsa@debian.org>
Uploaders: Peter Palfrader <weasel@debian.org>, Martin Zobel-Helas <zobel@debian.org>, Stephen Gran <sgran@debian.org>, Luca Filipozzi <lfilipoz@debian.org>
Build-Depends: debhelper (>= 10)
Standards-Version: 3.8.1

Package: debian.org
Architecture: all
Depends: userdir-ldap, zsh, tcsh, mksh, ksh, csh, locales-all, exim4-daemon-heavy | postfix, sudo, libpam-pwdfile, vim, nano, munin-node, gnupg, bzip2, cacert-spi | ca-certificates (>> 20080411), ed, cron, dialog, libterm-readline-gnu-perl, logrotate, syslog-ng (>= 3.1), nagios-plugins-standard (>= 1.4.15) | nagios-plugins-basic, debconf-utils, gawk
Recommends: debian.org-recommended
Conflicts: dchroot-dsa (<= 2:1)
Description: DSA's host metapackage
 This package depends on packages that should be installed on every
 debian.org host, and conflicts with packages that should never be
 installed.

Package: debian.org-recommended-linux
Architecture: all
Depends: vlan, ifenslave, bridge-utils, iproute2,
	strace,
	lsscsi,
	ethtool,
	bwm-ng,
	iputils-tracepath,
	traceroute,
	lshw,
	lsof,
	sysstat,
	apparmor,
Description: metapackage to pull in recommended, but not essential, tools for linux system
 This package depends on packages that are recommended to be installed on
 every Linux debian.org host but are not essential.

Package: debian.org-recommended
Architecture: any
Depends: debian.org,
	busybox-static,
	debian.org-recommended-linux [linux-any],
	molly-guard
	,
	ruby-msgpack
	,
	man-db
	,
	devscripts
	,
	rcs, git,
	mtr-tiny, tcpdump, tcptraceroute, ndisc6,
	bind9-host, dnsutils, whois, unbound-host
	,
	emacs-nox,
	mg,
	vim-scripts | base-files (>= 13), vim-addon-manager
	,
	at, uptimed,
	puppet,
		ruby-augeas | libaugeas-ruby,
		augeas-lenses (>= 0.7),
	dlocate
	,
	dsa-nagios-checks
	,
	munin-plugins-extra
	,
	dmidecode [amd64 arm64 armhf i386 riscv64],
	edac-utils,
	sg3-utils
	,
	ipython3
	,
	htop, psmisc,
	,
	ncdu,
	mc,
	tree,
	fd-find,
	,
	genisoimage,
	,
	less,
	rsync,
	byobu,
	screen,
	tmux,
	ldap-utils, ldapvi,
	mmv,
	moreutils,
	patchutils,
	diffstat,
	bc,
	netcat-openbsd, socat, telnet
	,
	deborphan | base-files (>= 13)
	,
	file
	,
	buffer
	,
	bsd-mailx
	,
	timeout | coreutils (>= 8.0-1)
	,
	wget, curl, lftp, ftp,
	,
	dput
	,
	uuid-runtime
	,
	finger
	,
	xz-utils,
	pigz,
	unzip
	,
	w3m,
	elinks,
	mutt
	,
	ccze,
	multitail,
	fish
	,
	jq
Recommends:
	ipython,
Description: metapackage to pull in recommended, but not essential, tools
 This package depends on packages that are recommended to be installed on
 every debian.org host but are not essential.

Package: debian.org-recommended-bullseye
Architecture: all
Depends: debian.org-recommended,
	foot-terminfo
Description: metapackage to pull in recommended, but not essential, bullseye tools
 This package depends on packages that are recommended to  be installed on
 every debian.org host running bullseye, but are not essential.

Package: debian.org-bugs.debian.org
Architecture: all
Depends: graphviz,
	libcgi-simple-perl,
	libgravatar-url-perl,
	libio-stringy-perl,
	liblist-moreutils-perl,
	libmail-rfc822-address-perl,
	libmime-tools-perl,
	libmldbm-perl,
	libparams-validate-perl,
	libsafe-hole-perl,
	libtext-template-perl,
	liburi-perl,
	libhtml-tree-perl,
	libsoap-lite-perl,
	libtext-iconv-perl,
	libdbd-pg-perl,
	libfile-libmagic-perl,
	libnet-dns-sec-perl,
	libwww-perl,
	imagemagick,
	libdatetime-format-mail-perl,
	libdatetime-format-pg-perl,
	libdbix-class-perl,
	libdbix-class-schema-loader-perl,
	libdbix-class-helpers-perl,
	libdbix-class-deploymenthandler-perl,
	libdbix-class-timestamp-perl,
	libterm-progressbar-perl,
	libtext-xslate-perl,
	libtext-iconv-perl,
	libmouse-perl,
	libmousex-nativetraits-perl,
	libaliased-perl,
	libstrictures-perl,
	libbareword-filehandles-perl,
	libindirect-perl,
	libmultidimensional-perl,
	liblist-allutils-perl,
	libmail-message-perl,
# contributors.debian.org's data mining
	python3-requests,
	python3-debian,
	python3-six
Description: metapackage for bugs.debian.org dependencies
 This package depends on everything that is needed for a bugs.debian.org
 mirror or (in the future) bugs-master.

Package: debian.org-bugs.debian.org-master
Architecture: all
Depends: debian.org-bugs.debian.org,
	gnuplot,
	libclamav-client-perl,
	fonts-liberation,
	lynx,
# smartsync
	python3-pyinotify,
	python3-yaml,
Description: metapackage for bugs-master.debian.org dependencies
 This package depends on everything that is needed for a
 bugs-master.debian.org

Package: debian.org-bugs.debian.org-search
Architecture: all
Depends: debian.org-bugs.debian.org,
	hyperestraier,
	libdate-manip-perl,
	libsearch-estraier-perl
Description: metapackage for bugs-search.debian.org dependencies
 This package depends on everything that is needed for a
 bugs-search.debian.org

Package: debian.org-bugs.debian.org-mx
Architecture: all
Depends: debian.org-bugs.debian.org,
	spamassassin,
	build-essential,
	spamc,
	libmail-spf-perl,
	re2c,
	libsys-syslog-perl,
	libio-socket-inet6-perl,
	libnet-ident-perl,
	libdbi-perl,
	pyzor,
	libmail-dkim-perl,
	libdbd-pg-perl,
	procmail
Description: metapackage for bugs.debian.org mail exchanger
 This package depends on everything that is needed for a bugs.debian.org
 MX.


Package: debian.org-packages-master.debian.org
Architecture: all
Depends: debian.org-packages.debian.org,
	procmail,
	gettext,
	libhtml-template-perl,
	libxml-rss-perl,
	libtimedate-perl,
	libio-stringy-perl,
	freecdb,
	tig
Description: metapackage for packages.debian.org dependencies
 This package depends on everything that is needed for a
 packages-master.debian.org server.  For the mirrors all you need is the
 debian.org-packages.debian.org package.

Package: debian.org-packages.debian.org
Architecture: all
Depends: liblocale-maketext-lexicon-perl,
	libi18n-acceptlanguage-perl,
	libnumber-format-perl,
	libcompress-zlib-perl,
	libhtml-parser-perl,
	libmldbm-perl,
	libtext-iconv-perl,
	libclass-accessor-perl,
	liburi-perl,
	libtemplate-perl,
	liblingua-stem-perl,
	libsearch-xapian-perl,
	libtimedate-perl,
	libcgi-fast-perl,
	libapache2-mod-fcgid,
	dpkg-dev,
	rsync
Description: metapackage for packages.debian.org dependencies
 This package depends on everything that is needed for a packages.debian.org
 mirror.  For packages-master install the debian.org-packages-master.debian.org
 package.

Package: debian.org-www-master.debian.org
Architecture: all
Depends: debiandoc-sgml,
	asciidoc,
	cm-super,
	debhelper,
	docbook,
	docbook-dsssl,
	docbook-utils,
	docbook-xsl,
	dpkg-dev,
	fonts-arphic-uming,
	fonts-mlym,
	gawk,
	gettext,
	ghostscript,
	imagemagick,
	iso-codes,
	isoquery,
	jadetex,
	kcc,
	latex-cjk-all,
	latex-cjk-chinese,
	ldap-utils,
	libcgi-pm-perl,
	libgd-perl,
	libemail-address-perl,
	libintl-perl,
	liblocale-codes-perl,
	libmime-lite-perl,
	libnet-ldap-perl,
	libsoap-lite-perl,
	libtimedate-perl,
	libwww-perl,
	libxml2-utils,
	libxml-rss-perl,
	locales, locales-all,
	lmodern,
	maildrop,
	opencc,
	openjade,
	opensp,
	po4a,
	poxml,
	procmail,
	python3-lxml,
	texlive-fonts-recommended,
	texlive-lang-all,
	latex-cjk-japanese,
	texlive-latex-base,
	texlive-latex-extra,
	texlive-latex-recommended,
	fonts-wqy-microhei,
	fonts-nanum,
	fonts-nanum-coding,
	fonts-noto-cjk,
	fonts-vlgothic,
	tidy,
	time,
	uni2ascii,
	wml,
	dblatex,
	xplanet,
	xplanet-images,
	xsltproc,
	libjson-perl,
	libxml-feedpp-perl,
	libyaml-tiny-perl,
	git-restore-mtime,
	latexmk,
	python3-distro-info,
	python3-sphinx,
	python3-stemmer,
	python3-sphinx-rtd-theme,
	tex-gyre,
	texinfo,
	fonts-freefont-otf,
Description: metapackage for www-master.debian.org dependencies
 This package depends on everything that is needed for a www-master.debian.org.


Package: debian.org-buildd.debian.org
Architecture: all
Depends:
	dose-builddebcheck,
	dose-distcheck,
	ghostscript,
	libcompress-bzip2-perl,
	libcompress-zlib-perl,
	libhash-merge-perl,
	libmailtools-perl,
	libdbi-perl,
	libdbd-pg-perl,
	libfile-spec-perl,
	libstring-escape-perl,
	libstring-format-perl,
	libyaml-tiny-perl,
	python3-apt,
	python3-coverage,
	python3-debian,
	python3-mako,
	python3-nose,
	python3-psycopg2,
	python3-sqlalchemy,
	r-base-core,
	r-cran-lattice,
	moreutils (>= 0.39~),
	libdpkg-perl,
	php,
	php-bz2,
	php-cgi,
	php-pgsql,
	libapache2-mod-auth-openidc,
	libapache2-mod-fcgid,
	libapache2-mod-wsgi-py3,
	libyaml-libyaml-perl,
	procmail
Description: metapackage for buildd.debian.org dependencies
 This package depends on everything that is needed for a buildd.debian.org.


Package: debian.org-patch-tracker.debian.org
Architecture: all
Depends: patchutils,
	python,
	python-cheetah,
	python-pygments,
	python-debian,
	reprepro,
	python-wsgiref,
	python-simplejson,
	python-pysqlite2,
	diffstat
Description: metapackage for patch-tracker.debian.org dependencies
 This package depends on everything that is needed for a patch-tracker.debian.org.

Package: debian.org-planet.debian.org
Architecture: all
Depends: libwww-perl,
	timeout | coreutils (>= 8.0-1),
	debianutils,
	python-django,
	python,
	libdbi-perl, libdate-manip-perl, libdbd-sqlite3-perl
Description: metapackage for planet.debian.org dependencies
 This package depends on everything that is needed for a planet.debian.org.

Package: debian.org-security-tracker.debian.org
Architecture: all
Depends: libdbd-sqlite3-perl,
	libterm-readline-gnu-perl,
	sqlite3,
	python3-apsw,
	python3-apt,
	python3-requests,
	jq
Description: metapackage for security-tracker.debian.org dependencies
 This package depends on everything that is needed for a security-tracker.debian.org.


Package: debian.org-ftpmaster.debian.org-code-signing
Architecture: all
Depends: devscripts,
	dpkg-dev,
	dput,
	libengine-pkcs11-openssl (>= 0.4.9-3) | libengine-pkcs11-openssl1.1,
	ykcs11,
	pesign,
	python3-requests,
	python3-sqlalchemy,
	python3-yaml,
# Needed for sign-file, any kbuild will do
	linux-kbuild-5.10 | linux-kbuild-4.19,
Description: metapackage for the code-signing service
 Currently co-located with ftpmaster, this service handles signing of kernels,
 modules, and UEFI executables to support Debian's participation in the UEFI
 Secure Boot programme.

Package: debian.org-ftpmaster.debian.org
Architecture: all
Depends: apt-utils,
	libapt-pkg-dev,
	libemail-sender-perl,
	libcgi-pm-perl,
	lintian,
	python3,
	python3-apt,
	python3-debianbts,
	python3-debian,
	python3-ldap,
	python3-psycopg2,
	python3-pyrss2gen,
	python3-rrdtool,
	python3-six,
	python3-sqlalchemy,
	python3-tabulate,
	python3-voluptuous,
	python3-yaml,
	binutils-multiarch,
	build-essential,
	graphviz,
	r-base,
	symlinks,
	unp,
	procmail,
	ruby,
	lynx,
	libxml-rss-perl,
	ghostscript,
	perforate,
	wdiff,
	ruby-soap4r,
	ack-grep,
	jdupes,
	python3-debiancontributors,
	libgfshare-bin,
	pinentry-curses,
	debmirror,
Description: metapackage for ftpmaster
 This package depends on everything that is needed for an ftpmaster setup.

Package: debian.org-ftpmaster.debian.org-api
Architecture: all
Depends:
	python3-bottle,
	libapache2-mod-wsgi-py3,
	debian.org-ftpmaster.debian.org,
Description: metapackage for ftpmaster api service
 This package depends on everything that is needed for the
 api.ftp-master.debian.org service.

Package: debian.org-ftpmaster.debian.org-poneys
Architecture: all
Depends:
	dpkg-dev-el,
	emacs-goodies-el,
	colordiff
Description: metapackage for ftpmaster extras
 Things not really needed needed.

Package: debian.org-upload.debian.org
Architecture: all
Depends: acl,
         python3-apt,
         python3-debian,
         python3-psycopg2,
         python3-sqlalchemy,
         python3-rrdtool,
         python3,
         libemail-sender-perl
Description: metapackage for upload-master host
 This package depends on everything that an upload master host (with
 queued and DEFERRED queue) needs.

Package: debian.org-release.debian.org
Architecture: all
Depends:
	colordiff,
	dose-distcheck,
	dose-builddebcheck,
	mutt,
	subversion,
	wdiff,
	libapt-pkg-perl,
	libapt-pkg-dev,
	dpkg-dev,
	procmail,
	rrdtool,
	python3-apt,
	python3-debian,
	python3-six,
	python3-sphinx,
	python3-yaml,
	picosat,
	git-email,
	postgresql-client,
	python3-psycopg2,
	python3-sqlalchemy,
	libdbd-pg-perl,
	libsoap-lite-perl,
	libjson-perl,
	ben,
Description: metapackage for release
 This package depends on everything that is needed for a release.debian.org
 setup.

Package: debian.org-cdbuilder.debian.org
Architecture: all
Depends: build-essential,
	debhelper,
	debian-cd,
	jigit,
	m4,
	tofrodos,
	qemu-system-arm,
	qemu-system-x86,
	qemu-utils,
	qemu-kvm,
	zip,
	zsync,
	fakeroot,
	dosfstools,
	xorriso,
	libsoap-lite-perl,
	mtools,
	syslinux-utils,
	libdbd-sqlite3-perl,
	libset-tiny-perl,
	libparallel-forkmanager-perl,
	libjson-perl,
	libfile-slurp-perl,
	libyaml-libyaml-perl,
	netpbm,
	mktorrent,
Description: metapackage for cdbuilder
 This package depends on all the packages that are needed for a cdbuilder setup.

Package: debian.org-cdimage-search.debian.org
Architecture: all
Depends: libconfigreader-simple-perl, libdbd-sqlite3-perl
Description: metapackage for cdimage-search.debian.org
 This package depends on all the packages that are needed for the CD/DVD search CGI website

Package: debian.org-manpages.debian.org
Architecture: all
Depends: golang-go,
	mandoc
Description: metapackage for manpages
 This package depends on all the packages that are needed for manpages.d.o setup.

Package: debian.org-nm.debian.org
Architecture: all
Depends: libapache2-mod-wsgi-py3,
	fonts-fork-awesome,
	libjs-bootstrap4,
	libjs-jquery-datatables,
	libjs-jquery-flot,
	mutt, mboxgrep,
	python3,
	python3-requests,
	python3-psycopg2,
	python3-django (>= 2:2.2),
	python3-django-filters,
	python3-django-housekeeping,
	python3-ldap3,
	python3-apt,
	python3-xapian,
	python3-dateutil,
	python3-debian,
	python3-debiancontributors,
	python3-tz,
	python3-git,
	python3-markdown,
	python3-djangorestframework,
	python3-requests-oauthlib,
	python3-gitlab,
	python3-jwcrypto,
	procmail,
	rrdtool,
	moreutils,
	libjs-jquery,
	postgresql-client,
	gettext,
Description: metapackage for nm.debian.org
 This package depends on all the packages that are needed for nm.debian.org setup.

Package: debian.org-backports.debian.org
Architecture: all
Depends: debian.org-ftpmaster.debian.org,
	libdbi-perl, libdbd-pg-perl,
	ikiwiki,
Description: metapackage for backports.debian.org
 This package depends on all the packages that are needed for backports.debian.org setup.

Package: debian.org-qa.debian.org
Architecture: all
Depends: db-util,
	devscripts (>= 2.15.9~),
	gnuplot-nox,
	wml,
	libgd-perl,
	libapt-pkg-perl,
		libcgi-pm-perl,
		libdbd-pg-perl,
		libdate-manip-perl,
		libdpkg-perl,
		libmailtools-perl,
		libsoap-lite-perl,
		libtemplate-perl,
		libxml-simple-perl,
		libmail-sendmail-perl,
		libdate-calc-perl,
		libnet-ldap-perl,
		libjson-perl,
		libyaml-syck-perl,
	php,
	php-cgi,
	php-cli,
	php-ldap,
	php-pgsql,
	php-xml,
	php-dba,
	libapache2-mod-fcgid,
	python3, python3-apt, python3-psycopg2,
	python3-bsddb3,
	python3-lxml,
	postgresql-client,
	rename,
	rrdtool,
	raptor2-utils,
	dose-distcheck, dose-builddebcheck, python3-yaml, python3-debian, python3-debianbts,
	brz,
	cvs,
	darcs,
	git,
	mercurial,
	subversion,
Description: metapackage for qa.debian.org
 This package depends on all the packages that are needed for qa.debian.org setup.

Package: debian.org-packages.qa.debian.org
Architecture: all
Depends: db4.7-util,
	libio-compress-perl,
	libio-compress-lzma-perl,
	libmailtools-perl,
	libmime-tools-perl,
	libmail-verp-perl,
	xsltproc,
	mhonarc,
	python,
	python-lxml,
	python-soappy,
	python-debian,
	python-yaml,
	python-zsi
Description: metapackage for packages.qa.debian.org
 This package depends on all the packages that are needed for packages.qa.debian.org setup.

Package: debian.org-tracker.debian.org
Architecture: all
Depends:
# Runtime
	daemon,
	libapache2-mod-wsgi-py3,
	python3-apt,
	python3-bs4,
	python3-debian,
	python3-debianbts (>= 2.6),
	python3-django (>= 3:4.2),
	python3-gpg,
	python3-jwcrypto,
	python3-requests (>= 2),
	python3-requests-oauthlib,
	python3-yaml,
	python3-pyinotify,
	dpkg-dev,
# Database access
	python3-psycopg2,
	postgresql-client,
# Test suite and docs
	python3-responses,
	python3-sphinx,
	python3-sphinx-rtd-theme,
Description: metapackage for tracker.debian.org
 This package depends on all the packages that are needed for tracker.debian.org.

Package: debian.org-udd.debian.org
Architecture: all
Depends: curl,
	dpkg-dev,
	graphviz,
	libapt-pkg-dev,
	libcrypt-ssleay-perl,
	libcgi-simple-perl,
	libdbd-pg-perl,
	libdbi-perl,
	ruby-debian,
	libhtml-template-perl,
	libio-stringy-perl,
	libjson-perl,
	liblist-allutils-perl,
	liblist-moreutils-perl,
	libmail-message-perl,
	libmime-tools-perl,
	libmldbm-perl,
	libparams-validate-perl,
	libperl-dev,
	libsafe-hole-perl,
	libtext-template-perl,
	libyaml-syck-perl,
	python3-apt,
	python3-dateutil,
	python3-launchpadlib,
	python3-psycopg2,
	python3-yaml,
	ruby-json,
	ruby-net-ssh,
	ruby-peach,
	ruby-oj
Description: metapackage for udd.debian.org
 This package depends on all the packages that are needed for udd.debian.org setup.

Package: debian.org-snapshot.debian.org
Architecture: all
Depends:
	ruby,
	ruby-pg,
	python3-yaml,
	python3-psycopg2,
	python3-fuse,
	uuid-runtime,
	python3-dev,
	gcc,
	python3-flask,
	python3-flask-mail,
	libapache2-mod-wsgi-py3,
Description: metapackage for snapshot.debian.org
 This package depends on all the packages that are needed for snapshot.debian.org setup.

Package: debian.org-women.debian.org
Architecture: all
Depends: wml
Description: metapackage for women.debian.org dependencies
 This package depends on everything that is needed for women.debian.org.


Package: debian.org-popcon.debian.org
Architecture: all
Depends:
	libchart-perl,
	libcgi-pm-perl
Description: metapackage for popcon.debian.org dependencies
 This package depends on everything that is needed for popcon.debian.org.

Package: debian.org-dde.debian.net
Architecture: all
Depends: python-apt,
	python-debian,
	python-debianbts,
	python-feedparser,
	python-flup,
	python-nose,
	python-paste,
	python-psycopg2,
	python-pyrss2gen,
	python-soappy,
	python-werkzeug,
	python-xapian,
	python-yaml
Description: metapackage for dde.debian.net dependencies
 This package depends on everything that is needed for dde.debian.net.

Package: debian.org-debtags.debian.org
Architecture: all
Depends: python3-apt,
	python3-debian,
	python3-django (>= 2:2.2),
	python3-django-housekeeping,
	python3-xapian (>= 1.2.7-1~bpo60+1),
	python3-psycopg2,
	python3-debiancontributors,
	python3-six,
	python3-jwcrypto,
	python3-requests-oauthlib,
	python3-gitlab,
	libjs-jquery,
	libjs-bootstrap4,
	fonts-fork-awesome,
	postgresql-client,
	g++,
	libapt-pkg-dev
Description: metapackage for debtags.debian.org dependencies
 This package depends on everything that is needed for debtags.debian.org.

Package: debian.org-sso.debian.org
Architecture: all
Depends: libapache2-mod-wsgi-py3,
	python3-requests,
	python3-django,
	python3-lockfile,
	python3-ldap3,
	python3-psycopg2,
	python3-openssl,
	gnutls-bin
Description: metapackage for sso.debian.org dependencies
 This package depends on everything that is needed for sso.debian.org.

Package: debian.org-lists.debian.org
Architecture: all
Depends: xapian-tools,
	xapian-omega,
	smartlist,
	procmail,
	gnuplot,
	rrdtool,
	libdpkg-perl,
	gettext,
	grepmail,
	libcompress-raw-zlib-perl,
	libgcrypt20-dev,
	libmoose-perl,
	libnet-oauth2-perl,
	carton,
	amavisd-new,
	amavis-lists,
	p7zip,
	unrar-free,
	pfqueue,
	pyzor,
	razor,
	mrtgutils,
	pax,
	pwgen,
	libmojolicious-perl,
	libmojo-sqlite-perl,
	libtext-table-perl,
	libdata-validate-email-perl,
	ccze,
	pflogsumm,
	gmime-bin,
	libgmime-3.0-dev,
Description: metapackage for lists.debian.org dependencies
 This package depends on everything that is needed for lists.debian.org.

Package: debian.org-ddtp.debian.org
Architecture: all
Depends: libalgorithm-diff-xs-perl,
	libdbd-pg-perl,
	libdbi-perl,
	libmime-tools-perl,
	libtext-diff-perl,
	libtext-iconv-perl,
	libwww-perl,
	libmime-tools-perl,
	gnuplot-nox,
	libapache2-mod-wsgi-py3,
	python3-setuptools,
	python3-psycopg2,
	python3-django,
	python3-sqlalchemy,
	tig,
	postgresql-client,
	libdata-validate-ip-perl,
	libtemplate-perl,
	libdigest-bcrypt-perl,
	libmath-random-secure-perl,
	libjson-perl,
	libcrypt-jwt-perl
Description: metapackage for ddtp.debian.org dependencies
 This package depends on everything that is needed for ddtp.debian.org.

Package: debian.org-i18n.debian.org
Architecture: all
Depends: gettext,
	po-debconf,
	libdpkg-perl,
	libfile-slurp-perl,
	liblocale-gettext-perl,
	liblocale-codes-perl,
	libmailtools-perl,
	libsoap-lite-perl,
	libtext-iconv-perl,
	libtimedate-perl,
	libwww-perl,
	tig,
	rrdtool
Description: metapackage for i18n.debian.org dependencies
 This package depends on everything that is needed for i18n.debian.org.

Package: debian.org-l10n.debian.org
Architecture: all
Depends: gettext,
	po-debconf,
	libdpkg-perl,
	libfile-slurp-perl,
	liblocale-gettext-perl,
	liblocale-codes-perl,
	libmailtools-perl,
	libsoap-lite-perl,
	libtext-iconv-perl,
	libtimedate-perl,
	libwww-perl,
	tig,
	rrdtool
Description: metapackage for l10n.debian.org dependencies
 This package depends on everything that is needed for l10n.debian.org.

Package: debian.org-piuparts-master.debian.org
Architecture: all
Depends: debhelper,
	asciidoc,
	libjs-sphinxdoc,
	lsb-release,
	git,
	xmlto,
	apache2,
	ghostscript,
	golang-go,
	r-base-dev,
	r-recommended,
	tango-icon-theme,
	screen,
	python3,
	python3-all,
	python3-apt,
	python3-debian,
	python3-debianbts (>= 3.0.2~),
	python3-distro-info,
	python3-nose,
	python3-rpy2,
	python3-setproctitle,
	python3-six,
	python3-sphinx,
	python3-yaml,
	libsoap-lite-perl,
	dose-distcheck,
	xz-utils
Description: metapackage for piuparts-master.debian.org
 This package depends on all the packages that are needed for the
 master in the piuparts.debian.org setup.

Package: debian.org-piuparts-slave.debian.org
Architecture: all
Depends: debhelper,
	asciidoc,
	git,
	xmlto,
	debootstrap,
	debsums,
	lsb-release,
	screen,
	python3,
	python3-all,
	python3-apt,
	python3-debian,
	python3-debianbts (>= 3.0.2~),
	python3-distro-info,
	python3-nose,
	python3-six,
	python3-sphinx,
	python3-yaml,
	adequate
Description: metapackage for piuparts-slave.debian.org
 This package depends on all the packages that are needed for the
 slaves in the piuparts.debian.org setup.

Package: debian.org-wiki.debian.org
Architecture: all
Depends: libapache2-mod-wsgi,
	libcgi-pm-perl,
	libjson-perl,
	python-moinmoin (>= 1.9.7-2~bpo7+1),
	python-launchpadlib,
	libsoap-lite-perl,
	python-docutils
Description: metapackage for wiki.debian.org
 This package depends on all the packages that are needed for the Debian wiki

Package: debian.org-vote.debian.org
Architecture: all
Depends: libparse-recdescent-perl,
	libmime-tools-perl,
	libnet-ldap-perl,
	libgnupg-interface-perl,
	libmail-gnupg-perl,
	graphviz
Description: metapackage for vote.debian.org
 This package depends on all the packages that are needed for DeVoteE

Package: debian.org-bits.debian.org
Architecture: all
Depends: pelican, python3-feedparser
Description: metapackage for bits.debian.org
 This package depends on all the packages that are needed for the Debian
 blog bits.debian.org

Package: debian.org-micronews.debian.org
Architecture: all
Depends: pelican, python3-feedparser, python3-feedgenerator
Description: metapackage for micronews.debian.org
 This package depends on all the packages that are needed for the Debian
 service micronews.debian.org

Package: debian.org-d-i.debian.org
Architecture: all
Depends: gnuplot,
	libdate-manip-perl,
	libemail-mime-perl,
	libemail-sender-perl,
	libfile-slurp-perl,
	libgd-graph-perl,
	libhtml-template-perl,
	liblist-moreutils-perl,
	libsort-versions-perl,
	myrepos,
	gettext,
	graphviz,
	fonts-liberation,
	dose-distcheck,
	liburi-perl,
	aspell,
	libunicode-string-perl,
	libxml-libxml-perl,
# /home/debian-cd/bin/push_di_to_cdbuild requires lockfile:
	procmail,
# Taken from d-i's manual (src:installation-guide)'s build-deps:
	docbook,
	docbook-xml,
	docbook-xsl,
	xsltproc,
	gawk,
	libhtml-parser-perl,
	w3m,
	poxml,
	jadetex,
	openjade | openjade1.3,
	dblatex,
	docbook-dsssl,
	ghostscript,
	texlive-xetex,
	lmodern,
	texlive-lang-chinese,
	texlive-lang-cyrillic,
	texlive-lang-czechslovak,
	texlive-lang-european,
	texlive-lang-french,
	texlive-lang-german,
	texlive-lang-greek,
	texlive-lang-italian,
	texlive-lang-korean,
	texlive-lang-other,
	texlive-lang-portuguese,
	texlive-lang-spanish,
	cm-super,
	fonts-wqy-microhei,
	fonts-vlgothic,
	fonts-nanum,
	fonts-nanum-coding,
	fonts-noto-cjk,
	fonts-freefont-ttf,
	po-debconf
Description: metapackage for d-i.debian.org
 This package depends on all the packages that are needed for the
 d-i.debian.org setup.

Package: debian.org-codesearch.debian.org
Architecture: all
Depends: dpkg-dev,
	postgresql-client
Description: metapackage for codesearch.debian.org
 This package depends on all the packages that are needed for the codesearch
 setup.

Package: debian.org-dedup.debian.net
Architecture: all
Depends: curl,
	python3,
	python3-apt,
	python3-arpy,
	python3-debian,
	python3-jinja2,
	python3-pil,
	python3-pkg-resources,
	python3-werkzeug,
	python3-yaml,
	python3-sqlalchemy,
	python3-ssdeep,
	python3-zstandard,
	sqlite3
Description: metapackage for dedup.debian.net
 This package depends on all packages that are required for operating the
 Debian duplication detector.

Package: debian.org-wnpp-by-tags.debian.net
Architecture: all
Depends:
 debtags,
 python3-debian,
 python3-bs4,
 python3-lxml,
Description: metapackage for wnpp-by-tags.debian.net
 This package depends on all the packages that are needed for the wnpp-by-tags
 setup.

Package: debian.org-blends.debian.org
Architecture: all
Depends: python3-genshi,
 python3-markdown,
 python3-docutils,
 python3-debian,
 python3-psutil,
 python3-psycopg2,
 python3-apt,
 postgresql-client
Description: metapackage for blends.debian.org
 This package depends on all the packages that are needed for the
 blends.debian.org setup.

Package: debian.org-pet.debian.net
Architecture: all
Depends: python-apt,
	python-argparse,
	python-debian,
	python-debianbts,
	python-inotifyx,
	python-paste,
	python-psycopg2,
	python-pyramid,
	python-popcon,
	python-sqlalchemy,
	python-subversion
Description: metapackage for pet.debian.net dependencies
 This package depends on everything that is needed for the PET setup.

Package: debian.org-dsa.debian.org
Architecture: all
Depends: ikiwiki, libtext-wikicreole-perl, libtext-wikiformat-perl, libtext-multimarkdown-perl
Description: metapackage for dsa.debian.org master dependencies
 This package depends on everything that is needed for the service

Package: debian.org-dns.debian.org
Architecture: all
Depends: perl-doc,
	nagios-nrpe-plugin,
	sqlite3,
	libdata-compare-perl,
	libnet-dns-perl,
	libnet-dns-sec-perl,
	dehydrated,
Description: metapackage for dns.debian.org master dependencies
 This package depends on everything that is needed for the service

Package: debian.org-nagios.debian.org
Architecture: all
Depends: icinga,
	nagios-plugins-contrib
Description: metapackage for nagios.debian.org master dependencies
 This package depends on everything that is needed for the service and
 also provides the dpkg-diverts for the REMOTE_USER hack

Package: debian.org-deriv.debian.net
Architecture: all
Depends: python3,
	python3-apt,
	python3-atomicwrites,
	python3-cairosvg,
	python3-debian,
	python3-magic,
	python3-pil,
	python3-psycopg2,
	python3-requests,
	python3-simplejson,
	python3-yaml,
	libxml-feed-perl,
	apt-transport-https,
	dpkg-dev,
	patchutils,
	rsync,
	curl,
	make,
	lzip,
Homepage: https://wiki.debian.org/Derivatives/Integration#Patches
Description: metapackage for the derivatives census patch generation
 This package depends on everything that is needed for the service

Package: debian.org-httpredir.debian.org
Architecture: all
Depends: inoticoming,
	libanyevent-perl,
	libanyevent-http-perl,
	libev-perl,
	libtimedate-perl,
	libgeo-ip-perl,
	libwww-perl,
	liburi-perl,
	libplack-perl,
	starman
Description: metapackage for http-redirector dependencies
 This package depends on everything that is needed for an instance of
 the http-redirector.

Package: debian.org-search.debian.org
Architecture: all
Depends: xapian-omega
Description: metapackage for search.debian.org dependencies
 This package depends on everything that is needed for an instance of
 the www search engine (aka search.debian.org).

Package: debian.org-dgit.debian.org
Architecture: all
Depends: perl,
	libwww-perl,
	libdpkg-perl,
	git,
	devscripts,
	dpkg-dev,
	libdigest-sha-perl,
	liblist-moreutils-perl,
	libtext-iconv-perl,
	libtext-glob-perl,
	libtext-csv-perl,
	dput,
	curl,
	libjson-perl,
	libwww-curl-perl,
	gpgv,
	chiark-utils-bin,
	libdbd-sqlite3-perl,
	sqlite3
Description: metapackage for dgit.debian.org dependencies
 This package depends on everything that is needed for an instance of
 the dgit git server (aka dgit.debian.org).
 .
 (This is actually simply the dependencies of the dgit and
 dgit-infrastructure binary packages, but due to chicken-and-egg
 problems and for ease of maintenance we are running the dgit
 infrastructure out of a git tree.)

Package: debian.org-appstream.debian.org
Architecture: all
Depends: gdc,
	libappstream-compose-dev,
	optipng,
	gettext,
	gobject-introspection,
	gperf,
	libgirepository1.0-dev,
	libglib2.0-dev,
	liblmdb-dev,
	libstemmer-dev,
	libxmlb-dev,
	libxml2-dev,
	libyaml-dev,
	meson,
	ldc,
	docbook-xsl,
	ffmpeg,
	gir-to-d,
	itstool,
	libarchive-dev,
	libcairo2-dev,
	libcurl4-gnutls-dev,
	libfontconfig-dev,
	libfreetype6-dev,
	libglibd-2.0-dev,
	libjs-highlight.js,
	libjs-jquery-flot,
	liblzma-dev,
	libpango1.0-dev,
	librsvg2-dev,
	xsltproc,
	gdb
Description: metapackage for appstream.debian.org dependencies
 This package depends on everything that is needed for an instance of
 the DEP-11/AppStream metadata extractor.

Package: debian.org-debian-r.debian.org
Architecture: all
Depends: debian.org-ftpmaster.debian.org
Description: metapackage for debian-r.debian.org dependencies
 This package depends on everything that is needed for the debian-r
 archive software (which is generally everything that is required by
 ftpmaster.debian.org).

Package: debian.org-ftpmaster.debian-ports.org
Architecture: all
Depends: apt-utils,
	git,
	gnupg,
	python3,
	python3-debian,
	procmail,
	wget
Description: metapackage for debian-ports ftpmaster
 This package depends on everything that is needed for a debian-ports
 ftpmaster setup (using mini-dak).

Package: debian.org-bootstrap.debian.net
Architecture: all
Depends: curl,
	debian-ports-archive-keyring,
	gnuplot,
	dose-distcheck,
	apt-cudf,
	botch,
	python3-pydot,
	libjs-jquery,
	libjs-jquery-tablesorter,
	libgraph-easy-perl,
	libsoap-lite-perl,
	time,
	python3-lxml,
	python3-pycurl,
Description: metapackage for bootstrap.debian.net
 This package depends on everything that is needed for the bootstrap
 analysis tools with results displayed on bootstrap.debian.net

Package: debian.org-mirror-master.debian.org
Architecture: all
Depends: sqlite3,
	python3-sqlalchemy,
	python3-dateutil,
	python3-jinja2,
	python3-bs4,
	python3-alembic (>= 0.8.7)
Description: metapackage for mirror-master.debian.org
 This package depends on everything that is needed for
 mirror-master.debian.org.

Package: debian.org-sources.debian.org
Architecture: all
Depends: apache2,
	diffstat,
	dpkg-dev,
	exuberant-ctags,
	libapache2-mod-wsgi-py3,
	libjs-highlight.js,
	libjs-jquery,
	python3-apt,
	python3-debian,
	python3-flask,
	python3-flaskext.wtf,
	python3-magic,
	python3-matplotlib,
	python3-psycopg2,
	python3-sqlalchemy,
	tango-icon-theme,
	sloccount
Description: metapackage for sources.debian.org
 This package depends on everything that is needed for
 sources.debian.org (Debsources).

Package: debian.org-veyepar.debian.org
Architecture: all
Depends: apache2,
	inkscape,
	bs1770gain,
	ffmpeg,
	git,
	postgresql-client,
	python3-psycopg2,
	melt,
	python3-pyrss2gen,
	python3-virtualenv,
	libapache2-mod-wsgi-py3
Description: metapackage for veyepar.debian.org
 This package depends on everything (hopefully) that is needed for
 veyepar.debian.org (one option for DebConf video review system)

Package: debian.org-sreview.debian.org
Architecture: all
Depends: apache2,
	bs1770gain,
	ffmpeg,
	fonts-noto-cjk,
	fonts-noto-cjk-extra,
	git,
	gridengine-client,
	gridengine-exec,
	inkscape,
	kgb-client,
	libemail-sender-perl,
	libemail-simple-perl,
	libjson-xs-perl,
	libmojo-pg-perl,
	libmojolicious-perl,
	libmojolicious-plugin-openapi-perl,
	libmoose-perl,
	libxml-rss-perl,
	postgresql-client,
	libdatetime-format-iso8601-perl,
	libdatetime-format-pg-perl,
	libtext-format-perl,
	libcryptx-perl,
	libemail-address-perl,
	libjs-bootstrap4,
	fonts-font-awesome,
Recommends:
	gridengine-master
Description: metapackage for sreview.debian.{org,net}
 This package depends on everything (hopefully) that is needed for
 sreview.debian.net, to become sreview.debian.org at some point (other option
 for DebConf video review system)

Package: debian.org-wafer.debconf.org
Architecture: all
Depends:
	memcached,
	nodejs (>= 6.11.2~dfsg-1) | nodejs-legacy,
	npm,
	python3,
	python3-diff-match-patch,
	python3-django (>= 1:1.11),
	python3-django-compressor,
	python3-django-jsonfield,
	python3-django-nose,
	python3-django-reversion,
	python3-djangorestframework,
	python3-markdown,
	python3-pil,
	python3-psycopg2,
	python3-requests,
	python3-tz,
	python3-venv,
	python3-wheel,
	python3-yaml,
	libjs-jquery,
Description: metapackage for wafer-based debconf.org websites
 This package depends on everything (hopefully) that is needed for
 wafertest.debconf.org, and similar.

Package: debian.org-btslinks.debian.org
Architecture: all
Depends:
	python3,
	python3-bs4,
	python3-debianbts,
	python3-html5lib,
	python3-lxml,
	python3-ldap,
	python3-rrdtool,
	python3-pytest,
	python3-pytest-xdist,
	python3-yaml,
	rrdtool,
Description: metapackage for bts-link
 This package depends on everything (hopefully) that is needed for
 running bts-link.

Package: debian.org-keyring.debian.org
Architecture: all
Depends:
        python3-distutils,
Description: metapackage for keyring
 This package depends on everything (hopefully) that is needed for
 keyring.d.o

Package: debian.org-jenkins.debian.org
Architecture: all
Depends:
	default-jre-headless,
	jenkins-job-builder,
	python3,
	python3-psycopg2,
Description: metapackage for jenkins.debian.org
 This package depends on everything (hopefully) that is needed for
 running jenkins.debian.org.

Package: debian.org-debdelta.debian.org
Architecture: all
Depends:
	debdelta,
	python3,
	python3-requests,
	python3-apt,
	python3-debian,
	python3-gnupg,
	python3-lockfile,
	python3-daemon,
	python3-numpy,
	python3-scipy,
	python3-matplotlib,
	xdelta3,
	bsdiff,
	xz-utils,
	gpg-agent,
	dpkg-dev,
	git,
	gnuplot,
Description: metapackage for debdelta server
 This package depends on everything (hopefully) that is needed for
 running the debdelta archive generation for debdeltas.debian.net

Package: debian.org-raspberrypi.debian.org
Architecture: all
Depends: binfmt-support,
	bmap-tools,
	debootstrap,
	dosfstools,
	fakemachine,
	kpartx,
	qemu-utils,
	qemu-user-static,
	time,
	vmdb2,
	python3,
	zerofree,
Description: metapackage for raspi.debian.net image-specs scripts dependencies
 This package depends on everything that is needed to build the images which
 were at raspi.debian.net for various models of the Raspberry Pi.
